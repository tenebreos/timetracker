using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Timetracker.Data;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddDefaultIdentity<IdentityUser>(options =>
    options.SignIn.RequireConfirmedAccount = false)
    .AddRoles<IdentityRole>()
    .AddEntityFrameworkStores<TimetrackerContext>();

builder.Services.AddControllersWithViews();

string connectionString = builder.Configuration.GetConnectionString("DefaultConnection");
var postgresqlVersion = new Version(builder.Configuration["PostgreSqlVersion"]);

builder.Services.AddDbContext<TimetrackerContext>(options =>
    options.UseLazyLoadingProxies()
            .UseNpgsql(connectionString, options =>
                options.SetPostgresVersion(postgresqlVersion)));

if (builder.Environment.IsDevelopment())
{
    builder.Services.AddDatabaseDeveloperPageExceptionFilter();
    builder.Services.Configure<IdentityOptions>(options =>
    {
        options.Password.RequireDigit = false;
        options.Password.RequiredLength = 4;
        options.Password.RequiredUniqueChars = 0;
        options.Password.RequireNonAlphanumeric = false;
        options.Password.RequireUppercase = false;
    });
}

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseMigrationsEndPoint();
    using (var scope = app.Services.CreateScope())
    {
        var services = scope.ServiceProvider;

        var defaultPassword = builder.Configuration
            .GetValue<string>("DefaultUserPassword");

        var context = services.GetRequiredService<TimetrackerContext>();
        var userManager = services.GetRequiredService<UserManager<IdentityUser>>();
        var roleManager = services.GetRequiredService<RoleManager<IdentityRole>>();

        TimetrackerInitializer
            .Initialize(context, userManager, roleManager, defaultPassword);
    }
}
else
{
    app.UseExceptionHandler("/Home/Error");
}

Console.WriteLine($"# Runtime environment: {app.Environment.EnvironmentName}");

var prefix = app.Configuration["PathPrefix"];
if (!string.IsNullOrEmpty(prefix))
{
    app.UsePathBase(prefix);
}

app.UseStaticFiles();

app.UseRouting();

app.UseAuthentication();
app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.MapRazorPages();

app.Run();