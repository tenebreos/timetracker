using Microsoft.AspNetCore.Identity;
using Timetracker.Models;

namespace Timetracker.Data;

public class TimetrackerInitializer
{
    public const string Email = "avicavrof@asdf.com";
    public const string RoleName = "Admin";

    public static async void Initialize(TimetrackerContext context,
                                  UserManager<IdentityUser> userManager,
                                  RoleManager<IdentityRole> roleManager,
                                  string defaultPassword)
    {
        if (context.Profiles.Any())
        {
            return;
        }

        // Add a default profile
        var users = new List<Profile>
        {
            new Profile { Email = Email }
        };
        users.ForEach(t => context.Profiles.Add(t));

        IdentityResult result;

        // Add a default user
        var user = await userManager.FindByNameAsync(Email);
        if (user == null)
        {
            var newUser = new IdentityUser
            {
                UserName = Email,
                EmailConfirmed = true
            };
            result = await userManager.CreateAsync(newUser, defaultPassword);
            if (!result.Succeeded)
            {
                throw new Exception($"Error adding user: {result.Errors}");
            }

            // Add the default user to an administrator role
            if (!await roleManager.RoleExistsAsync(RoleName))
            {
                result = await roleManager.CreateAsync(new IdentityRole(RoleName));
            }
            result = await userManager.AddToRoleAsync(newUser, RoleName);
        }

        context.SaveChanges();
    }
}