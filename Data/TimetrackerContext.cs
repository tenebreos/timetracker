﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Principal;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

using Timetracker.Models;
using Task = Timetracker.Models.Task;

namespace Timetracker.Data;

public class TimetrackerContext : IdentityDbContext
{
    public TimetrackerContext(DbContextOptions<TimetrackerContext> options)
        : base(options)
    {
    }

    public DbSet<Tag> Tags { get; set; }
    public DbSet<Task> Tasks { get; set; }
    public DbSet<Profile> Profiles { get; set; }

    public Profile FetchProfile(IIdentity identity)
    {
        return Profiles.Single(p => p.Email == identity.Name);
    }
}