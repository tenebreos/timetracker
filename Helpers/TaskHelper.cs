﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Timetracker.Models;
using Task = Timetracker.Models.Task;

namespace Timetracker.Helpers
{
    public static class TaskHelper
    {
        public static void AddTag(this Task task, Tag tag)
        {
            if (!task.Tags.Contains(tag))
            {
                task.Tags.Add(tag);
                tag.TotalTime += task.Duration;
            }
        }
        public static void RemoveTag(this Task task, Tag tag)
        {
            if (task.Tags.Contains(tag))
            {
                task.Tags.Remove(tag);
                tag.TotalTime -= task.Duration;
            }
        }

        public static string IntToBase32String(long i)
        {
            const string alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            StringBuilder sb = new StringBuilder();
            do
            {
                sb.Insert(0, alphabet[(int)(i % 32)]);
                i = i / 32;
            } while (i != 0);
            return sb.ToString();
        }
    }
}