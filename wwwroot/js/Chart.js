function ChartTags(canvasId, data) {
    ChartUsingChartJs(canvasId, data);
}

function InitChart(canvasId) {
    InitChartJs(canvasId);
}


let barchart = null;


function InitChartJs(canvasId) {
    let config = {
        type: 'bar',
        data: {
            labels: [],
            datasets: []
        },
        options: {
            indexAxis: 'y',
            scales: {
                x: {
                    beginAtZero: true,
                    title: {
                        display: true,
                        text: 'Time spent [h]'
                    }
                }
            }
        }
    }
    barchart = new Chart(
        document.getElementById(canvasId),
        config
    );
}


function ChartUsingChartJs(canvasId, data) {
    if (barchart == null) {
        InitChartJs(canvasId)
    }
    let labels = []
    let datapoints = []
    for (let tagSummary of data.summary) {
        labels.push(tagSummary.tagName)
        let time = tagSummary.timeSummary.split(":").map(parseFloat)
        let hours = time[0] + time[1] / 60 + time[2] / 3600;
        datapoints.push(hours)
    }
    barchart.data.labels = labels
    barchart.data.datasets = [{
        label: 'Time spent',
        backgroundColor: 'rgb(109, 14, 169)',
        borderColor: 'rgb(237, 12, 54)',
        data: datapoints
    }]
    barchart.update()
}
