"use strict";

const HourMs = 60 * 60 * 1000;
const MinuteMs = 60 * 1000;
const SecondMs = 1000;

/* 
 * A class encapsulating logic of a stopwatch.
 */
class Stopwatch {

    constructor(clockfaceId, startButtonId, startButtonCallback, stopButtonId, stopButtonCallback) {
        this.clockface = document.getElementById(clockfaceId);
        this.startButton = document.getElementById(startButtonId);
        this.stopButton = document.getElementById(stopButtonId);
        this.timeStarted = null;
        this.state = {
            seconds: 0,
            minutes: 0,
            hours: 0
        };
        this.timeoutHandle = null;


        this.startButton.onclick = startButtonCallback;
        this.stopButton.onclick = stopButtonCallback;

        this.clear();
    }

    increment() {

        let deltaT = Date.now() - this.timeStarted;

        this.state.hours = Math.floor(deltaT / HourMs);
        deltaT %= HourMs;
        this.state.minutes = Math.floor(deltaT / MinuteMs);
        deltaT %= MinuteMs;
        this.state.seconds = Math.floor(deltaT / SecondMs);

        this.updateClockface();
        this.timeoutHandle = null;

        this.start();
    }

    updateClockface() {
        const state = this.state;
        const fmt = function (int) {
            return int ? (int > 9 ? int : "0" + int) : "00";
        };
        this.clockface.textContent =
            `${fmt(state.hours)}:${fmt(state.minutes)}:${fmt(state.seconds)}`;
    }

    start() {
        if (this.timeStarted == null) {
            this.timeStarted = Date.now();
        }
        if (this.timeoutHandle == null) {
            this.timeoutHandle = setTimeout(() => this.increment(), 1000);
        }
    }

    stop() {
        clearTimeout(this.timeoutHandle);
        this.timeoutHandle = null;
    }

    clear() {
        this.clockface.textContent = "00:00:00";
        this.timeStarted = null;
        this.state.seconds = 0;
        this.state.minutes = 0;
        this.state.hours = 0;
    }

}


/* 
 * A class encapsulating logic of a timer.
 */
class Timer {

    timeoutEvent = null;
    initialState = 0;
    state = 0;
    startTime = null;
    autoStop = false;
    clockface = null;

    constructor(clockfaceId) {
        this.clockface = document.getElementById(clockfaceId);
    }

    set(hours, minutes, seconds) {
        this.state = hours * HourMs;
        this.state += minutes * MinuteMs;
        this.state += seconds * SecondMs;
        this.initialState = this.state;
        this.update();
    }

    decrement() {
        if (this.autoStop && this.state == 0) {
            return;
        }

        let deltaT = Date.now() - this.startTime;
        // console.log(deltaT);
        // console.log(this.startTime)
        this.state = this.initialState - deltaT;

        this.update();
        this.start();
    }

    start() {
        this.timeoutEvent = setTimeout(() => this.decrement(), 1000);
        if (this.startTime == null) {
            this.startTime = Date.now();
        }
    }

    stop() {
        clearTimeout(this.timeoutEvent);
    }

    clear() {
        this.state = 0;
        this.startTime = null;
        this.update();
    }

    update() {
        this.clockface.textContent = this.renderClockface(this.state);
    }

    renderClockface(state) {
        let remainder = state;
        let negative = false;
        if (remainder < 0) {
            remainder *= -1;
            negative = true;
        }

        const hours = Math.floor(remainder / HourMs);
        remainder %= HourMs;
        const minutes = Math.floor(remainder / MinuteMs);
        remainder %= MinuteMs;
        const seconds = Math.floor(remainder / SecondMs);

        const fmt = function (n) {
            return n ? (n > 9 ? n : "0" + n) : "00";
        };

        return `${negative ? '-' : ''}${fmt(hours)}:${fmt(minutes)}:${fmt(seconds)}`;
    }

    getDuration() {
        return this.renderClockface(this.initialState - this.state);
    }

    setAutostop(isEnabled) {
        this.autoStop = isEnabled;
    }
}

