﻿async function SendTask(url, csrfToken, task) {
    //let data = {
    //    StartTime: "12-08-2021T07:58:31Z",
    //    Duration: "01:25:00",
    //    TagIds: [2, 3]
    //}
    return fetch(url, {
        method: 'POST',
        headers: {
            RequestVerificationToken: csrfToken,
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(task)
    })
}

async function FetchTags(url, csrfToken, request) {
    return fetch(url, {
        method: 'POST',
        headers: {
            RequestVerificationToken: csrfToken,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(request)
    })
}

async function FetchTasks(url, csrfToken, request) {
    return fetch(url, {
        method: 'POST',
        headers: {
            RequestVerificationToken: csrfToken,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(request)
    })
}
    