﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Timetracker.Models;

public class Tag
{
    public int Id { get; set; }

    [Required]
    [Display(Name = "Tag Name")]
    //[RegularExpression(@"^\S")] --- TODO: fixme
    public string Name { get; set; }
    
    [Display(Name = "Time Summary")]
    [DataType(DataType.Duration)]
    public TimeSpan TotalTime { get; set; }

    [Display(Name = "Tasks")]
    public virtual ICollection<Task>? Tasks { get; set; }

    [Display(Name = "User")]
    public virtual Profile User { get; set; }

    public Tag(Profile user, string name)
    {
        Name = name;
        User = user;
        Tasks = new List<Task>();
        TotalTime = TimeSpan.Zero;
    }

    public Tag() { }
}