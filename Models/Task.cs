﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Timetracker.Models
{
    public class Task
    {
        public int Id { get; set; }

        public string Name { 
            get
            {
                return Tags.First().Name + "#" + Helpers.TaskHelper.IntToBase32String(StartTime.Ticks);
            }
        }

        [DataType(DataType.DateTime)]
        [Display(Name = "Started At")]
        public DateTime StartTime { get; set; }

        [DataType(DataType.Duration)]
        [Display(Name = "Duration")]
        public TimeSpan Duration { get; set; }

        [DataType(DataType.DateTime)]
        [Display(Name = "Ended At")]
        public DateTime EndTime { get { return StartTime + Duration; } }

        [Display(Name = "User")]
        public virtual Profile User { get; set; }

        [Display(Name = "Tags")]
        public virtual ICollection<Tag> Tags { get; set; }

        public Task()
        {
            Tags = new List<Tag>();
        }
    }
}