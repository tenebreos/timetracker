﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Timetracker.Models
{
    public class Profile
    {
        public int Id { get; set; }

        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        public virtual ICollection<Task> Tasks { get; set; }
        public virtual ICollection<Tag> Tags { get; set; }

    }
}