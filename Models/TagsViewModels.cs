﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Timetracker.Models
{
    public class TagDetailsViewModel
    {
        public Tag Tag { get; set; }
        public ICollection<Task> Tasks { get; set; }
    }

    public class TaskCreateViewModel
    {
        [DataType(DataType.DateTime)]
        [Display(Name = "Start Time")]
        public DateTime StartTime { get; set; }

        [DataType(DataType.Duration)]
        [Display(Name = "Duration")]
        public TimeSpan Duration { get; set; }

        [Display(Name = "Tags")]
        public List<int> TagIds { get; set; }

        [Range(-11, 14)]
        public double UtcOffset { get; set; }  // In hours.

        public List<Tag>? AvailableTags { get; set; }
    }

    public class TaskEditViewModel
    {
        public int TaskId { get; set; }

        [Display(Name = "Tags")]
        public List<int> TagIds { get; set; }

        public List<SelectListItem>? AvailableTags { get; set; }
    }

    public class TaskSubmitViewModel
    {
        [DataType(DataType.DateTime)]
        [Display(Name = "Start Time")]
        public DateTime StartTime { get; set; }

        [DataType(DataType.Duration)]
        [Display(Name = "Duration")]
        public TimeSpan Duration { get; set; }

        [Display(Name = "Tags")]
        public List<int> TagIds { get; set; }

        public List<Tag>? AvailableTags { get; set; }
    }

    public class TagCreateViewModel
    {
        [Display(Name = "Tag Name")]
        public string TagName { get; set; }
    }
    
    public class AnalysisFetchTagsViewModel
    {
        [DataType(DataType.Date)]
        public DateTime StartTime { get; set; }
        
        [DataType(DataType.Date)]
        public DateTime EndTime { get; set; }

        public List<int> TagIds { get; set; }

        public string FilterType { get; set; }
    }
    
    public class TagSummary
    {
        public string TagName { get; set; }
        public TimeSpan TimeSummary { get; set; }
    }

    public class TagEditViewModel 
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class AnalysisFetchTasksViewModel
    {
        [DataType(DataType.DateTime)]
        public DateTime StartTime { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime EndTime { get; set; }
    }

    public class AboutViewModel
    {
        public int TasksTracked { get; set; }
        public int TagsDefined { get; set; }
        public int UsersRegistered { get; set; }
    }

}