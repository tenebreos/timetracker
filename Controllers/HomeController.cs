﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Timetracker.Models;
using Timetracker.Data;

namespace Timetracker.Controllers;

public class HomeController : Controller
{
    private readonly ILogger<HomeController> _logger;
    private readonly TimetrackerContext _dbContext;

    private readonly TimeSpan EmptyTimeSpan = new TimeSpan(0);

    public HomeController(ILogger<HomeController> logger, TimetrackerContext dbContext)
    {
        _logger = logger;
        _dbContext = dbContext;
    }

    public IActionResult Index()
    {
        return View();
    }

    public IActionResult Privacy()
    {
        return View();
    }

    public IActionResult About()
    {
        AboutViewModel model = new AboutViewModel
        {
            TagsDefined = _dbContext.Tags.Count(),
            UsersRegistered = _dbContext.Profiles.Count(),
            TasksTracked = _dbContext.Tasks.Count()
        };
        return View(model);
    }

    public IActionResult Contact()
    {
        ViewBag.Message = "Your contact page.";

        return View();
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
}