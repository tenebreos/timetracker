﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using Timetracker.Data;
using Timetracker.Models;
using X.PagedList;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;

namespace Timetracker.Controllers
{
    [Authorize]
    public class TagsController : Controller
    {
        private TimetrackerContext _dbContext;

        public TagsController(TimetrackerContext dbContext)
        {
            _dbContext = dbContext;
        }

        // GET: Tags
        public IActionResult Index(string order, string currentFilter, string searchString, int? page)
        {
            const string NAME = "";
            const string NAME_DESC = "NameDesc";
            const string TIME = "Time";
            const string TIME_DESC = "TimeDesc";

            ViewBag.CurrentSort = order;
            ViewBag.NameSortParm = string.IsNullOrEmpty(order) ? NAME_DESC : NAME;
            ViewBag.TimeSortParm = order == TIME ? TIME_DESC : TIME;

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            var profile = _dbContext.FetchProfile(User.Identity);
            IEnumerable<Tag> tags = profile.Tags.AsEnumerable();
            if (!String.IsNullOrEmpty(searchString))
            {
                tags = tags.Where(s => s.Name.Contains(searchString));
            }
            switch (order)
            {
                case NAME_DESC:
                    tags = tags.OrderByDescending(t => t.Name);
                    break;
                case TIME:
                    tags = tags.OrderBy(t => t.TotalTime);
                    break;
                case TIME_DESC:
                    tags = tags.OrderByDescending(t => t.TotalTime);
                    break;
                default:
                    tags = tags.OrderBy(t => t.Name);
                    break;
            }
            int pageSize = 25;
            int pageNumber = (page ?? 1);
            return View(tags.ToPagedList(pageNumber, pageSize));
        }

        // GET: Tags/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return BadRequest();
            }
            var profile = _dbContext.FetchProfile(User.Identity);
            Tag tag = profile.Tags.Single(t => t.Id == id);
            var tasks = profile.Tasks.Where(task => task.Tags.Contains(tag)).ToList();
            if (tag == null)
            {
                return NotFound();
            }
            return View(new TagDetailsViewModel
            {
                Tag = tag,
                Tasks = tasks
            });
        }

        // GET: Tags/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Tags/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(TagCreateViewModel model)
        {
            if (ModelState.IsValid)
            {
                Profile profile = _dbContext.FetchProfile(User.Identity);
                Tag tag = new Tag(profile, model.TagName);
                profile.Tags.Add(tag);
                _dbContext.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(model);
        }

        // GET: Tags/Edit/5
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return BadRequest();
            }
            Tag? tag = _dbContext.Tags.Find(id);
            if (tag == null)
            {
                return NotFound();
            }
            var viewModel = new TagEditViewModel 
            {
                Name = tag.Name,
                Id = tag.Id
            };
            return View(viewModel);
        }

        // POST: Tags/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit([Bind("Id,Name")] TagEditViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                Tag? tag = _dbContext.Tags.Find(viewModel.Id);
                if (tag == null)
                {
                    return NotFound();
                }
                tag.Name = viewModel.Name;
                _dbContext.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(viewModel);
        }

        // GET: Tags/Delete/5
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return BadRequest();
            }
            Tag tag = _dbContext.Tags.Find(id);
            if (tag == null)
            {
                return NotFound();
            }
            return View(tag);
        }

        // POST: Tags/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            Tag tag = _dbContext.Tags.Find(id);
            _dbContext.Tags.Remove(tag);
            _dbContext.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
