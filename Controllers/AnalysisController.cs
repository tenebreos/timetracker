﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Timetracker.Data;
using Timetracker.Models;

namespace Timetracker.Controllers
{
    [Authorize]
    public class AnalysisController : Controller
    {

        private TimetrackerContext _db;

        public AnalysisController(TimetrackerContext context)
        {
            this._db = context;
        }

        // GET: Analysis/Summary
        public ActionResult Summary()
        {
            Profile user = _db.FetchProfile(User.Identity);
            return View(user.Tags);
        }

        // GET: Analysis/Calendar
        public ActionResult Calendar()
        {
            return View();
        }

        // TODO: The following should be a GET request.
        // POST: Analysis/FetchTags
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FetchTags(
            [FromBody]
            [Bind("StartTime","EndTime","TagIds","FilterType")]
            AnalysisFetchTagsViewModel request
        )
        {
            if (!ModelState.IsValid) 
            {
                var errors = ModelState.Select(x => x.Value.Errors)
                           .Where(y=>y.Count>0)
                           .ToList();
                return BadRequest();
            }

            Profile user = _db.FetchProfile(User.Identity);
            List<TagSummary> pairs = new List<TagSummary>();
            List<Tag> tags;
            if (request.FilterType == "whitelist")
            {
                tags = user.Tags.Where(t => request.TagIds.Contains(t.Id)).ToList();
            } 
            else 
            {
                if (request.TagIds != null)
                {
                    tags = user.Tags.Where(t => !request.TagIds.Contains(t.Id)).ToList();
                }
                else
                {
                    tags = user.Tags.ToList();
                }
            }

            foreach (Tag tag in tags)
            {
                TimeSpan d = user.Tasks
                    .Where(t => t.StartTime.CompareTo(request.StartTime) >= 0 
                             && t.EndTime.CompareTo(request.EndTime) < 0 
                             && t.Tags.Any(tg => tg.Id == tag.Id))
                    .Aggregate(new TimeSpan(), (acc, task) => acc.Add(task.Duration));
                pairs.Add(new TagSummary { TagName = tag.Name, TimeSummary = d });
            }
            return Json(new { Summary = pairs });
        }

        // TODO: The following should be a GET request.
        // POST: Analysis/FetchTasks
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FetchTasks(
            [FromBody]
            [Bind("StartTime,EndTime")]
            AnalysisFetchTasksViewModel request
        )
        {
            Profile user = _db.FetchProfile(User.Identity);
            var tasks = user.Tasks
                .Where(t => t.StartTime.CompareTo(request.StartTime.ToUniversalTime()) >= 0
                    && t.StartTime.CompareTo(request.EndTime.ToUniversalTime()) < 0)
                .Select(t => new
                {
                    Id = t.Id,
                    Name = t.Name,
                    StartTime = DateTime.SpecifyKind(t.StartTime, DateTimeKind.Utc).ToString("O"),
                    EndTime = DateTime.SpecifyKind(t.EndTime, DateTimeKind.Utc).ToString("O"),
                    Tags = t.Tags.Select(tag => tag.Name).ToList()
                })
                .ToList();
            return Json(new { Tasks = tasks });
        }
    }
}