﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Web;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using Timetracker.Data;
using Timetracker.Models;
using Timetracker.Helpers;

using Task = Timetracker.Models.Task;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Timetracker.Controllers
{
    [Authorize]
    public class TasksController : Controller
    {
        private TimetrackerContext db;

        public TasksController(TimetrackerContext db)
        {
            this.db = db;
        }

        // GET: Tasks
        public ActionResult Index(string order)
        {
            const string START_DATE = "";
            const string START_DATE_DESC = "StartDateDesc";
            const string DURATION = "Duration";
            const string DURATION_DESC = "DurationDesc";
            const string END_DATE = "EndTime";
            const string END_DATE_DESC = "TimeDesc";

            ViewBag.StartDateParm = string.IsNullOrEmpty(order) ? START_DATE_DESC : START_DATE;
            ViewBag.DurationParm = order == DURATION ? DURATION_DESC : DURATION;
            ViewBag.EndDateParm = order == END_DATE ? END_DATE_DESC : END_DATE;

            var profile = db.FetchProfile(User.Identity);
            IOrderedEnumerable<Task> tasks;
            switch (order)
            {
                case START_DATE_DESC:
                    tasks = profile.Tasks.OrderByDescending(t => t.StartTime);
                    break;
                case DURATION:
                    tasks = profile.Tasks.OrderBy(t => t.Duration);
                    break;
                case DURATION_DESC:
                    tasks = profile.Tasks.OrderByDescending(t => t.Duration);
                    break;
                case END_DATE:
                    tasks = profile.Tasks.OrderBy(t => t.EndTime);
                    break;
                case END_DATE_DESC:
                    tasks = profile.Tasks.OrderByDescending(t => t.EndTime);
                    break;
                default:
                    tasks = profile.Tasks.OrderBy(t => t.StartTime);
                    break;
            }
            return View(tasks.ToList());
        }

        // GET: Tasks/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return BadRequest();
            }
            var profile = db.FetchProfile(User.Identity);
            Task task = profile.Tasks.First(t => t.Id == id);
            if (task == null)
            {
                return NotFound();
            }
            return View(task);
        }

        // GET: Tasks/Create
        public ActionResult Create()
        {
            var profile = db.FetchProfile(User.Identity);
            TaskCreateViewModel model = new TaskCreateViewModel 
            { 
                AvailableTags = profile.Tags.ToList()
            };
            return View(model);
        }

        // POST: Tasks/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(
            [Bind("StartTime","Duration","TagIds","UtcOffset")] 
            TaskCreateViewModel data)
        {
            if (data.TagIds == null || data.TagIds.Count() < 1) 
            {
                ModelState.AddModelError("TagIds", "Number of Tags ona a Task must be > 1.");
            }

            var profile = db.FetchProfile(User.Identity);
            if (ModelState.IsValid) 
            {
                List<Tag> tags = profile.Tags.Where(t => data.TagIds.Contains(t.Id)).ToList();
                var startTime = DateTime.SpecifyKind(data.StartTime.AddHours(-data.UtcOffset), DateTimeKind.Utc);
                Task task = new Task
                {
                    StartTime = startTime,
                    Duration = data.Duration,
                    User = profile
                };
                tags.ForEach(t => task.AddTag(t));
                profile.Tasks.Add(task);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            data.AvailableTags = profile.Tags.ToList();
            return View(data);
        }

        // POST: Tasks/Submit
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Submit(
            [FromBody]
            [Bind("StartTime","Duration","TagIds")] 
            TaskSubmitViewModel data)
        {
            if (data.TagIds == null || data.TagIds.Count() < 1)
            {
                ModelState.AddModelError("TagIds", "Number of Tags on a Task must be > 1.");
            }

            if (ModelState.IsValid)
            {
                var profile = db.FetchProfile(User.Identity);
                List<Tag> tags = profile.Tags.Where(t => data.TagIds.Contains(t.Id)).ToList();
                Task task = new Task
                {
                    StartTime = data.StartTime,
                    Duration = data.Duration,
                    User = profile
                };
                tags.ForEach(t => task.AddTag(t));
                profile.Tasks.Add(task);
                db.SaveChanges();
                return Ok();
            }

            return BadRequest();
        }

        // GET: Tasks/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return BadRequest();
            }

            var profile = db.FetchProfile(User.Identity);
            Task task = profile.Tasks.First(t => t.Id == id);

            if (task == null)
            {
                return NotFound();
            }

            var viewModel = new TaskEditViewModel
            {
                TaskId = task.Id,
                AvailableTags = profile.Tags
                    .Select(t => new SelectListItem { 
                        Text = t.Name, 
                        Value = t.Id.ToString(),
                    })
                    .ToList(),
                TagIds = task.Tags.Select(e => e.Id).ToList()
            };
            return View(viewModel);
        }

        // POST: Tasks/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind("TaskId,TagIds")] TaskEditViewModel model, int? id)
        {
            if (id == null)
            {
                return BadRequest();
            }

            if (id != model.TaskId)
            {
                return NotFound();
            }

            var profile = db.FetchProfile(User.Identity);
            Task task = profile.Tasks.First(t => t.Id == model.TaskId);

            if (task == null)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                var tags = model.TagIds
                    .Select(id => db.Tags.Single(t => t.Id == id))
                    .ToList();
                var existingTags = task.Tags.ToList();
                foreach (var tag in existingTags)
                {
                    task.RemoveTag(tag);
                }
                foreach (var tag in tags)
                {
                    task.AddTag(tag);
                }
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            model.AvailableTags = profile.Tags
                .Select(t => new SelectListItem { 
                    Text = t.Name, 
                    Value = t.Id.ToString(),
                })
                .ToList();
            return View(model);
        }

        // GET: Tasks/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return BadRequest();
            }
            Task task = db.Tasks.Find(id);
            if (task == null)
            {
                return NotFound();
            }
            return View(task);
        }

        // POST: Tasks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var profile = db.FetchProfile(User.Identity);
            Task task = profile.Tasks.First(t => t.Id == id);
            db.Tasks.Remove(task);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // GET: Tasks/Measure
        [HttpGet]
        public ActionResult Measure()
        {
            var profile = db.FetchProfile(User.Identity);
            return View(profile.Tags);
        }
    }
}
